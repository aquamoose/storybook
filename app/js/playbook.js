var default_size = {
    width: 0,
    height: 0
};
var space = {
    top: 120,
    bottom: 100,
    left: 30,
    right: 30
};
var current_p = 1;
var p_count;
var scale = 1;
var page_width, page_height;
var auto_flip_delay = 2000;
var bookdata_json_url = "";
var blank_page = "";
var sound_array = new Array();
var sound_start_time, sound_current_time;
var is_single_page = true;
var is_auto_play = false;
var is_subtitle_on = true;
var is_dubbling_on = true;
var is_bgmusic_on = true;
var is_fullscreen_on = false;
var current_text_type = "";
var default_single = true;
var preload_all = false;
var dubbling_timer;
var subtitle_timer;
var flip_timer;
var is_img_ready = false;
var is_music_ready = false;
var is_sound_ready = false;
var bgmusic, dubbling_file, page_array;
var p_layout;
var waitForFinalEvent = (function () {
    var a;
    return function (c, b) {
        if (a != undefined) {
            clearTimeout(a)
        }
        a = setTimeout(c, b)
    }
})();
var lastWindowHeight = $(window).height();
var lastWindowWidth = $(window).width();
var jscrollpane_api;

function init(setting_enum) {
    window.log = function (msg) {
        if (window.console) {
            // console.log(msg)
        }
    };
    for (var k in setting_enum) {
        switch (k) {
            case "flip_delay":
                auto_flip_delay = eval("setting_enum." + k);
                break;
            case "width":
                page_width = eval("setting_enum." + k);
                break;
            case "height":
                page_height = eval("setting_enum." + k);
                break;
            case "preload_all":
                preload_all = eval("setting_enum." + k);
                break;
            case "bookdata_json_url":
                bookdata_json_url = eval("setting_enum." + k);
                break;
            case "text":
                current_text_type = eval("setting_enum." + k);
                break;
            case "blank":
                blank_page = eval("setting_enum." + k);
                break;
            case "single_page_mode":
                default_single = eval("setting_enum." + k);
                break;
            case "margin":
                space = eval("setting_enum." + k);
                break
        }
    }
    if (/iPad|iPhone|Android/i.test(navigator.userAgent)) {
        $("#btnFullscreen").hide();
        space.right = 50
    }
    var inner_w = window.innerWidth || document.documentElement.clientWidth;
    var inner_h = window.innerHeight || document.documentElement.clientHeight;
    var max_w = inner_w - space.left - space.right;
    var max_h = inner_h - space.bottom - space.top;
    var ratio_w = max_w / page_width;
    var ratio_h = max_h / page_height;
    var ratio = ratio_w > ratio_h ? ratio_h : ratio_w;
    default_size.width = Math.round(page_width * ratio);
    default_size.height = Math.round(page_height * ratio);
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        is_fullscreen_on = true
    }
    $("#wait_block").show();
    log("[json] begin loading: " + bookdata_json_url);
    $.support.cors = true;
    $.ajax({
        contentType: "application/json; charset=utf-8",
        url: bookdata_json_url,
        dataType: "json"
    }).done(function (data) {
        log("[json] book_data.json loaded");
        bgmusic = data.bgmusic;
        dubbling_file = data.dubbling;
        page_array = data.pages;
        p_count = page_array.length;
        p_layout = data.page_layout;
        is_single = data.is_single;
        var text_type_array = ["none", "text_ch", "text_py", "text_en"];
        for (var i = 0; i < page_array.length; i++) {
            for (var j = 0; j < text_type_array.length; j++) {
                if (page_array[i][text_type_array[j]] == undefined || page_array[i][text_type_array[j]] == "") {
                    page_array[i][text_type_array[j]] = blank_page
                }
            }
        }
        if (/Firefox/i.test(navigator.userAgent)) {
            var reg = /(\w+)\.mp3/;
            var matches;
            if (bgmusic != "") {
                bgmusic = bgmusic.replace(".mp3", ".ogg")
            }
            if (dubbling_file != "") {
                dubbling_file = dubbling_file.replace(".mp3", ".ogg")
            }
            var file = "";
            for (var i = 0; i < page_array.length; i++) {
                if (page_array[i]["mp3"] != undefined && page_array[i]["mp3"].length > 0) {
                    file = page_array[i]["mp3"][0]["file"];
                    if (file != undefined && file != "") {
                        page_array[i]["mp3"][0]["file"] = file.replace(".mp3", ".ogg")
                    }
                }
            }
        }
        var accumulated_time = 0;
        for (var i = 0; i < page_array.length; i++) {
            if (page_array[i]["mp3"] != undefined && page_array[i]["mp3"].length > 0) {
                page_array[i]["mp3"][0]["start_time"] = accumulated_time / 1000;
                var soundlen = parseFloat(page_array[i]["mp3"][0]["len"]) * 1000;
                accumulated_time += soundlen
            }
        }
        var i = 0;
        var img = page_array[i]["img"];
        var text = page_array[i][current_text_type];
        while (img == "") {
            i++;
            img = page_array[i]["img"];
            var text = page_array[i][current_text_type]
        }
        $("#page_left_img").attr("src", img);
        $("#page_left_img").hide();
        $("#page_left_text").attr("src", text);
        $("#page_left_text").hide();
        console.log(data);
        if(p_layout === 'v') {
            $("#page_block").css({
                width: (1 + default_size.width) + "px",
                height: (1 + default_size.height) + "px"
            });
            $("#page_block_container").css({
                width: (3 + default_size.width) + "px",
                height: (3 + default_size.height) + "px"
            });

            if(default_size.width > default_size.height) {
                $("#page_left_img, #page_left_text").width(default_size.width).height(default_size.height);

                $("#page_right_img, #page_right_text").width(default_size.width).height(default_size.height);
            }
            $("#page_left_img, #page_left_text").width(default_size.width).height(default_size.height);

            $("#page_right_img, #page_right_text").width(default_size.width).height(default_size.height);
            var left_space = (parseInt($("#page_block").css("width")) - parseInt($("#subtitle_block").css("width"))) / 2;
            $("#subtitle_block").css({
                left: left_space + "px"
            });
            scale = parseInt($("#page_block").css("width")) / page_width;

        } else {
            $("#page_block").css({
                width: (1 + default_size.height) + "px",
                height: (1 + default_size.width) + "px"
            });
            $("#page_block_container").css({
                width: (3 + default_size.height) + "px",
                height: (3 + default_size.width) + "px"
            });

            if(default_size.width > default_size.height) {
                $("#page_left_img, #page_left_text").width(default_size.width).height(default_size.height);

                $("#page_right_img, #page_right_text").width(default_size.width).height(default_size.height);
            }
            $("#page_left_img, #page_left_text").width(default_size.height).height(default_size.width);

            $("#page_right_img, #page_right_text").width(default_size.height).height(default_size.width);
            var left_space = (parseInt($("#page_block").css("width")) - parseInt($("#subtitle_block").css("width"))) / 2;
            $("#subtitle_block").css({
                left: left_space + "px"
            });
            scale = parseInt($("#page_block").css("width")) / page_width;
        }
        
        if (/iPad|iPhone|Android/i.test(navigator.userAgent)) {
            $(".flipbookcontent, .interactioncontent").hide();
            $(".preload-btn button").show().on("click", function () {
                loadMusic();
                $(".preload-btn").remove();
                start();
                resizeImgToFullWindow();
                resizeWindowToDefault();
            })
        } else {
            $(".preload-btn, #btnStart").remove();
            $("#wait_block").show();
            loadMusic();
            start()
        }
        if (!default_single || is_single === "1") {
            togglePageMode()
        }
        // if (is_single === "1") {
        //     console.log("not single")
        // } else if (is_single !== "1") {
        //     console.log("is single")
        // }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        log("[json] book_data.js loading failed! " + textStatus + " " + errorThrown)
    });
    $("#page_left_img, #page_right_img, #page_left_text, #page_right_text, #subtitle_block").hide();
    $("#btnStart").on("click", start);
    $("#btnPlay").on("click", togglePlayMode);
    $("#btnZoomin").on("click", zoomIn);
    $("#btnZoomout").on("click", zoomOut);
    $("#btnPageMode").on("click", togglePageMode);
    $("#btnFirst").on("click", firstPage);
    $("#btnPrevious").on("click", prevPage);
    $("#btnNext").on("click", nextPage);
    $("#btnLast").on("click", lastPage);
    $("#btnSound").on("click", toggleMusic);
    $("#btnSpeak").on("click", toggleSound);
    $("#btnSubtitle").on("click", toggleSubtitle);
    $("#btnFullscreen").on("click", toggleFullScreen);
    $(".toolmenu").hide();
    $(window).resize(function () {
        if ($(window).height() != lastWindowHeight || $(window).width() != lastWindowWidth) {
            lastWindowHeight = $(window).height();
            lastWindowWidth = $(window).width();
            waitForFinalEvent(resizeImgToFullWindow, 500)
        }
    });
    resizeImgToFullWindow();

    // $("#page_block").css({
    //     width: (1 + default_size.height) + "px",
    //     height: (1 + default_size.width) + "px"
    // });
    // $("#page_block_container").css({
    //     width: (3 + default_size.height) + "px",
    //     height: (3 + default_size.width) + "px"
    // });

    // if(default_size.width > default_size.height) {
    //     $("#page_left_img, #page_left_text").width(default_size.width).height(default_size.height);

    //     $("#page_right_img, #page_right_text").width(default_size.width).height(default_size.height);
    // }
    // $("#page_left_img, #page_left_text").width(default_size.height).height(default_size.width);

    // $("#page_right_img, #page_right_text").width(default_size.height).height(default_size.width);
    // var left_space = (parseInt($("#page_block").css("width")) - parseInt($("#subtitle_block").css("width"))) / 2;
    // $("#subtitle_block").css({
    //     left: left_space + "px"
    // });
    // scale = parseInt($("#page_block").css("width")) / page_width;
    // var element = $("#page_block_container").jScrollPane({
    //     autoReinitialise: true
    // });
    // jscrollpane_api = element.data("jsp")
    resizeWindowToDefault();
    // zoomIn();

}

function resizeWindowToDefault() {
    var a = default_size.width + space.left + space.right;
    var b = default_size.height + space.bottom + space.top;
    if (/MSIE/.test(navigator.userAgent) || /FIREFOX/i.test(navigator.userAgent)) {
        a += 50;
        b += 100
    } else {
        if (/Safari/i.test(navigator.userAgent) && !/Chrome/i.test(navigator.userAgent)) {
            a += 60;
            b += 100
        } else {
            b += 50
        }
    }
    if (a < 1000) {
        a = 1000
    }
    window.resizeTo(a, b);
    log(navigator.userAgent);
    log("initial window size: " + a + " x " + b)
}

function firstPage() {
    current_p = 1;
    while (is_single_page && page_array[current_p - 1]["img"] == "" && current_p < page_array.length && current_p >= 1) {
        current_p++
    }
    showPage()

}

function prevPage() {
    if (is_single_page && current_p > 1) {
        current_p--
    } else {
        if (!is_single_page && current_p > 2) {
            current_p -= 2
        } else {
            return
        }
    }
    while (is_single_page && page_array[current_p - 1]["img"] == "" && current_p <= page_array.length && current_p > 1) {
        current_p--
    }
    if (page_array[current_p - 1]["img"] == "") {
        firstPage()
    } else {
        showPage()
    }
}

function nextPage() {
    if (is_single_page && current_p < p_count) {
        current_p++
    } else {
        if (!is_single_page && current_p < p_count - 1) {
            current_p += 2
        } else {
            return
        }
    }
    while (is_single_page && page_array[current_p - 1]["img"] == "" && current_p <= page_array.length && current_p > 1) {
        current_p++
    }
    showPage()
}

function lastPage() {
    if (!is_single_page && p_count % 2 == 0) {
        current_p = p_count - 1
    } else {
        current_p = p_count
    }
    showPage()
}

function showPage() {
    clearTimeout(flip_timer);
    clearTimeout(dubbling_timer);
    clearTimeout(subtitle_timer);
    stopSound("done");
    stopSubtitle();
    $("#page_left_img, #page_left_text").hide();
    $("#page_right_img, #page_right_text").hide();
    $("#wait_block").show();
    load_resources()
}

function load_resources() {
    is_img_ready = false;
    $("#page_left_img").hide().attr("src", page_array[current_p - 1]["img"]);
    $("#page_left_text").hide().attr("src", page_array[current_p - 1][current_text_type]);
    if (!is_single_page && page_array.length > current_p) {
        $("#page_right_img").hide().attr("src", page_array[current_p]["img"]);
        $("#page_right_text").hide().attr("src", page_array[current_p][current_text_type])
    } else {
        $("#page_right_img, #page_right_text").hide().attr("src", "")
    }
    if (preload_all && $(".cache").length == 0) {
        var a = ["img", "text_ch", "text_py", "text_en"];
        for (var d = 0; d < page_array.length; d++) {
            for (var c = 0; c < a.length; c++) {
                var b = page_array[d][a[c]];
                if (b != undefined && b != "" && b != blank_page) {
                    log("preload " + b);
                    $("<img class='cache' src='" + b + "' width='0' height='0' style='display:none;'>").appendTo("body")
                }
            }
        }
        $(".cache").imagesLoaded(function () {
            is_img_ready = true;
            check_all_ready()
        })
    } else {
        $("#page_left_img, #page_right_img, #page_left_text, #page_right_text").imagesLoaded(function () {
            is_img_ready = true;
            check_all_ready()
        })
    }
    loadSound(current_p);
    loadSubtitle(current_p)
}

function check_all_ready() {
    log("[image and sound] check if all images and sounds are loaded");
    if (is_img_ready && is_sound_ready) {
        $("#wait_block").hide();
        $("#page_left_img").trigger("beforeShow").fadeIn();
        $("#page_left_text").fadeIn();
        if (!is_single_page && page_array.length > current_p) {
            $("#page_right_img").trigger("beforeShow").fadeIn();
            $("#page_right_text").fadeIn()
        }
        playNextSound();
        showNextSubtitle();
        if (!preload_all) {
            var a = is_single_page ? current_p : current_p + 1;
            if (page_array.length > a) {
                log("preload " + page_array[a]["img"]);
                $("<img class='cache' src='" + page_array[a]["img"] + "' width='0' height='0' style='display:none;'>").appendTo("body");
                if (!is_single_page && page_array.length > a + 1) {
                    log("preload " + page_array[a + 1]["img"]);
                    $("<img class='cache' src='" + page_array[a + 1]["img"] + "' width='0' height='0' style='display:none;'>").appendTo("body")
                }
            }
        }
    }
}

function togglePlayMode() {
    if (is_auto_play) {
        is_auto_play = false;
        clearTimeout(flip_timer)
    } else {
        is_auto_play = true;
        if ((!is_subtitle_on && !is_dubbling_on) || ($("#subtitle_block>span[status='playing']").length == 0 && $("#sound_block>audio[status='playing']").length == 0)) {
            flip_timer = setTimeout(nextPage, auto_flip_delay)
        }
    }
    $("#btnPlay").toggleClass("active");
    $("#btnPlay").trigger("switch")
}

function zoomIn() {
    var c = Math.round(page_width * scale * 0.1),
        a = Math.round(page_height * scale * 0.1);
    if (!is_single_page) {
        c = c * 2
    }
    var b = window.innerWidth || document.documentElement.clientWidth;
    var d = window.innerHeight || document.documentElement.clientHeight;
    console.log(window.innerWidth)
    console.log(screen.width)
    if (is_fullscreen_on) {
        resizeImg(3 / 2, false)
        if (window.innerWidth >= screen.width - 100 || window.innerHeight >= screen.height) {
            $("#page_block_container").addClass('overflow-important')
        }
        
        // $("#page_block_container, .jspContainer, .jspPane, #page_block").width(fsWidth)
        // $("#page_left_img, #page_left_text, #page_right_img, #page_right_text").css('transform', 'scale(1.1)')
    } else {
        if (b + c > screen.width || d + a > screen.height) {
            // toggleFullScreen();
            window.resizeBy(c, a)
            resizeImg(3 / 2, true)
            if (window.innerWidth >= screen.width - 100 || window.innerHeight >= screen.height) {
                $("#page_block_container").addClass('overflow-important')
            }
            // jscrollpane_api.destroy()
            // $('#btnZoomin').prop("disabled",true);
        } else {
            resizeImg(3 / 2, true)
            window.resizeBy(c, a)
            if (window.innerWidth >= screen.width - 100 || window.innerHeight >= screen.height) {
                $("#page_block_container").addClass('overflow-important')
            }
            // jscrollpane_api.destroy()
        }
    }
}

function zoomOut() {
    $('#btnZoomin').prop("disabled",false);
    var c = Math.round(page_width * scale * -0.05),
        a = Math.round(page_height * scale * -0.05);
    if (!is_single_page) {
        c = c * 2
    }
    var b = window.innerWidth || document.documentElement.clientWidth;
    var d = window.innerHeight || document.documentElement.clientHeight;
    console.log()
    if (is_fullscreen_on) {
        // if (jscrollpane_api != undefined && jscrollpane_api.getContentWidth() > $("#page_block").width()) {
        if (true) {
            resizeImg(2 / 3, true);
            $(".jspPane").css("left", "0px");
            window.resizeBy(c, a);
            is_fullscreen_on = false;
            if (window.innerWidth < screen.width - 100 || window.innerHeight < screen.height) {
                $("#page_block_container").removeClass('overflow-important')
            }
        } else {
            window.resizeBy(c, a);
            is_fullscreen_on = false;
            console.log(is_fullscreen_on)
            $("#btnFullscreen").toggleClass("active");
            $("#btnFullscreen").trigger("switch")
        }
    } else {
        resizeImg(2 / 3, true);
        window.resizeBy(c, a)
        if (window.innerWidth < screen.width - 100 || window.innerHeight < screen.height) {
            $("#page_block_container").removeClass('overflow-important')
        }
    }
}

function resizeImgToFullWindow() {
    var d = window.innerWidth || document.documentElement.clientWidth;
    var g = window.innerHeight || document.documentElement.clientHeight;
    var f = d - space.left - space.right;
    var a = g - space.bottom - space.top;
    var b = f / parseInt($("#page_block").css("width"));
    var e = a / parseInt($("#page_block").css("height"));
    var c = b > e ? e : b;
    resizeImg(c)
}

function resizeImg(f, e) {
    var g = Math.round(parseInt($("#page_block").css("width")) * f);
    var h = Math.round(parseInt($("#page_block").css("height")) * f);
    if (e == undefined || e == true) {
        // if (jscrollpane_api != undefined) {
        //     jscrollpane_api.destroy()
        // }
        $("#page_block").css({
            width: (1 + g) + "px",
            height: (1 + h) + "px"
        });
        $("#page_block_container").css({
            width: (3 + g) + "px",
            height: (3 + h) + "px"
        });
        // if (jscrollpane_api != undefined) {
        //     var d = $("#page_block_container").jScrollPane({
        //         autoReinitialise: false
        //     });
        //     jscrollpane_api = d.data("jsp")
        // }
    } else {
        $("#page_block").css({
            width: (1 + g) + "px",
            height: (1 + h) + "px"
        });
        // if (jscrollpane_api != undefined) {
        //     jscrollpane_api.reinitialise()
        // }
    }
    var c = is_single_page ? g : Math.floor(g / 2);
    var a = Math.round(c / parseInt($("#page_left_img").width()) * parseInt($("#page_left_img").height()));
    $("#page_left_img, #page_left_text").width(c).height(a);
    $("#page_right_img, #page_right_text").width(c).height(a);
    var b = (parseInt($("#page_block").css("width")) - parseInt($("#subtitle_block").css("width"))) / 2;
    $("#subtitle_block").css({
        left: b + "px"
    });
    scale = parseInt($("#page_block").css("width")) / page_width
}

function loadMusic() {
    log("[background music] begin loading: " + bgmusic);
    if (typeof bgmusic == "string" && bgmusic != "") {
        $('<audio id="bgmusic" name="bgmusic" src="' + bgmusic + '"></audio>').appendTo("#bgmusic_block");
        sound_array.bgmusic = new MediaElement("bgmusic", {
            loop: true,
            success: function (a, b) {
                a.load()
            }
        });
        sound_array.bgmusic.addEventListener("loadeddata", load_music_callback, false);
        sound_array.bgmusic.addEventListener("ended", playMusic, false);
        $("#wait_block").show()
    } else {
        load_music_callback()
    }
}

function load_music_callback() {
    log("[background music] loaded");
    if (sound_array.bgmusic != undefined && sound_array.bgmusic != null) {
        sound_array.bgmusic.removeEventListener("loadeddata", load_music_callback, false);
        playMusic()
    }
    $(".preload-btn").remove();
    $(".flipbookcontent, .interactioncontent").show();
    $("#page_left_img, #page_left_text").imagesLoaded(function () {
        $("#wait_block").hide();
        $("#page_left_img").trigger("beforeShow").show();
        $("#page_left_text").show();
        $("#btnStart").show()
    })
}

function playMusic() {
    if (typeof sound_array.bgmusic == "object") {
        if (sound_array.bgmusic.ended) {
            sound_array.bgmusic.setCurrentTime(0)
        }
        sound_array.bgmusic.play()
    }
}

function stopMusic() {
    if (typeof sound_array.bgmusic == "object") {
        sound_array.bgmusic.pause()
    }
}

function toggleMusic() {
    if (is_bgmusic_on) {
        is_bgmusic_on = false;
        stopMusic()
    } else {
        is_bgmusic_on = true;
        playMusic()
    }
    $("#btnSound").toggleClass("active");
    $("#btnSound").trigger("switch")
}

function toggleSound() {
    log("[sound] togggle sound playing");
    if (is_dubbling_on) {
        is_dubbling_on = false;
        stopSound("muted")
    } else {
        is_dubbling_on = true;
        playSound()
    }
    $("#btnSpeak").toggleClass("active");
    $("#btnSpeak").trigger("switch")
}

function loadSubtitle(b) {
    log("[subtitle] load all subtitles for current pages");
    clearTimeout(subtitle_timer);
    $("#subtitle_block>span").remove();
    for (var a = 0; page_array[b - 1].subtitle != undefined && a < page_array[b - 1].subtitle.length; a++) {
        $('<span class="subtitle p1" len="' + page_array[b - 1].subtitle[a]["len"] + '">' + page_array[b - 1].subtitle[a]["text"] + "</span>").hide().appendTo("#subtitle_block")
    }
    if (!is_single_page && page_array.length > b && page_array[b].subtitle != undefined) {
        for (var a = 0; a < page_array[b].subtitle.length; a++) {
            $('<span class="subtitle p2" len="' + page_array[b].subtitle[a]["len"] + '">' + page_array[b].subtitle[a]["text"] + "</span>").hide().appendTo("#subtitle_block")
        }
    }
}

function showNextSubtitle() {
    log("[subtitle] show next");
    clearTimeout(subtitle_timer);
    $("#subtitle_block>span[status='playing']").attr("status", "done");
    var b = $("#subtitle_block>span[status='done']").filter(":last").length > 0 ? ($("#subtitle_block>span[status='done']").filter(":last").next().length > 0 ? $("#subtitle_block>span[status='done']").filter(":last").next() : undefined) : ($("#subtitle_block>span").length > 0 ? $("#subtitle_block>span").eq(0) : undefined);
    $("#subtitle_block>span[status='done']").hide();
    if (b == undefined) {
        $("#subtitle_block").hide();
        if (is_auto_play) {
            flip_timer = setTimeout(nextPage, auto_flip_delay)
        }
        return
    }
    b.attr("status", "playing");
    b.show();
    if (is_subtitle_on && b.html() != "") {
        $("#subtitle_block").show()
    }
    var a = parseFloat(b.attr("len")) * 1000;
    subtitle_timer = setTimeout(showNextSubtitle, a)
}

function stopSubtitle() {
    log("[subtitle] stop");
    $("#subtitle_block").hide()
}

function toggleSubtitle() {
    if (is_subtitle_on) {
        is_subtitle_on = false;
        $("#subtitle_block").hide()
    } else {
        is_subtitle_on = true;
        if ($("#subtitle_block>span[status='playing']").length > 0 && $("#subtitle_block>span[status='playing']").eq(0).html() != "") {
            $("#subtitle_block").show()
        }
    }
    $("#btnSubtitle").toggleClass("active");
    $("#btnSubtitle").trigger("switch")
}

function togglePageMode() {
    if (is_single_page) {
        is_single_page = false
    } else {
        is_single_page = true
    }
    $("#btnPageMode").toggleClass("active");
    $("#btnPageMode").trigger("switch");
    var b = 0;
    var a = 0;
    var default_page = $("#page_left_img").height();
    if (is_single_page) {
        b = parseInt($("#page_block").css("height")) * 2;
        a = parseInt($("#page_block").css("width"))
    } else {
        b = Math.round(parseInt($("#page_block").css("height")) / 2);
        a = Math.round(parseInt($("#page_block").css("width")) / 2)
    }
    $("#page_block").css("height", b);
    $("#page_block_container").css("height", parseInt($("#page_block").css("height")) + 2 + "px");
    $("#page_block_container").css("width", parseInt($("#page_block").css("width")) + 2 + "px");
    $("#page_left_img, #page_right_img, #page_left_text, #page_right_text").width(a).height(b);
    if (current_p % 2 == 0) {
        current_p--
    }
    if (is_single_page && page_array[current_p - 1]["img"] == "") {
        current_p++
    }
    showPage();
    // resizeWindowToDefault();
    resizeImgToFullWindow()
}

function toggleFullScreen() {
    if (is_fullscreen_on == false) {
        window.moveTo(0, 0);
        window.resizeTo(screen.width, screen.height);
        is_fullscreen_on = true
    } else {
        resizeWindowToDefault();
        is_fullscreen_on = false
    }
    $("#btnFullscreen").toggleClass("active");
    $("#btnFullscreen").trigger("switch")
}

function start() {
    firstPage();
    $("#btnStart").remove();
    $(".toolmenu").show();
    if (/iPad|iPhone|Android/i.test(navigator.userAgent)) {
        $(".toolzoom").hide()
    }
}

function changeTextType(a) {
    current_text_type = a;
    $("#page_left_text").attr("src", page_array[current_p - 1][current_text_type]);
    if (!is_single_page && page_array.length > current_p) {
        $("#page_right_text").attr("src", page_array[current_p][current_text_type])
    }
};
