function loadSound(a) {
    clearTimeout(dubbling_timer);
    if (sound_array.dubbling == undefined) {
        is_sound_ready = false;
        log("[sound] begin loading: " + dubbling_file);
        if (typeof dubbling_file == "string" && dubbling_file != "") {
            $("#wait_block").show();
            $('<audio id="dubbling" name="dubbling" src="' + dubbling_file + '"></audio>').appendTo("#sound_block");
            sound_array.dubbling = new MediaElement("dubbling", {
                success: function (b, c) {
                    b.load();
                    if (/iPad|iPhone/i.test(navigator.userAgent)) {
                        if (window.applicationCache.status == 1) {
                            b.addEventListener("loadeddata", load_sound_callback, false)
                        } else {
                            b.addEventListener("progress", getAudioProgress, false)
                        }
                    } else {
                        b.addEventListener("loadeddata", load_sound_callback, false)
                    }
                }
            })
        } else {
            is_sound_ready = true
        }
    }
    $("#sound_block>span[class='dubbling']").remove();
    if (page_array[a - 1]["mp3"] != undefined && page_array[a - 1]["mp3"].length > 0) {
        $('<span class="dubbling"  name="s1" id="s1" start_time="' + page_array[a - 1]["mp3"][0]["start_time"] + '" len="' + page_array[a - 1]["mp3"][0]["len"] + '" ready="true"></span>').appendTo("#sound_block")
    }
    if (!is_single_page && page_array.length > a && page_array[a]["mp3"] != undefined && page_array[a]["mp3"].length > 0) {
        $('<span class="dubbling"  name="s2" id="s2" start_time="' + page_array[a]["mp3"][0]["start_time"] + '" len="' + page_array[a]["mp3"][0]["len"] + '" ready="true"></span>').appendTo("#sound_block")
    }
    check_all_ready()
}

function getAudioProgress(a) {
    var c = a.target || a.srcElement;
    var b = c.buffered.end(c.buffered.length - 1);
    log("progess of " + c.src + ", endBuf=" + b);
    if (Math.round(b / c.duration * 100) >= 95) {
        c.removeEventListener("progress", getAudioProgress, false);
        load_sound_callback(a)
    }
}

function load_sound_callback(a) {
    var c = a.target || a.srcElement;
    var b = c.src;
    log("[sound] loaded: " + b);
    c.removeEventListener("loadeddata", load_sound_callback, false);
    is_sound_ready = true;
    check_all_ready()
}

function playNextSound() {
    log("[sound] play next sound");
    stopSound("done");
    var d = $("#sound_block>span[class='dubbling'][status='done']").length > 0 ? ($("#sound_block>span[class='dubbling'][status='done']").filter(":last").next().length > 0 ? $("#sound_block>span[class='dubbling'][status='done']").filter(":last").next() : undefined) : ($("#sound_block>span[class='dubbling']").length > 0 ? $("#sound_block>span[class='dubbling']").eq(0) : undefined);
    sound_start_time = new Date().getTime();
    if (d == undefined) {
        if (/Android/i.test(navigator.userAgent) && is_bgmusic_on) {
            stopMusic();
            playMusic()
        }
        return
    }
    var b = parseFloat(d.attr("start_time")) * 1000;
    var c = parseFloat(d.attr("len")) * 1000;
    if (is_dubbling_on) {
        try {
            sound_array.dubbling.setCurrentTime(b / 1000)
        } catch (a) { }
        sound_array.dubbling.play();
        dubbling_timer = setTimeout(playNextSound, c);
        d.attr("status", "playing")
    } else {
        dubbling_timer = setTimeout(playNextSound, c);
        d.attr("status", "muted")
    }
}

function stopSound(a) {
    if ($("#sound_block>span[class='dubbling']").filter("[status='playing'],[status='muted']").length > 0) {
        log("[sound] stop sound");
        sound_array.dubbling.pause();
        $("#sound_block>span[class='dubbling']").filter("[status='playing'],[status='muted']").attr("status", a);
        if (a == "muted" && /Android/i.test(navigator.userAgent) && is_bgmusic_on) {
            stopMusic();
            playMusic()
        }
    }
}

function playSound() {
    if ($("#sound_block>span[class='dubbling'][status='muted']").length > 0) {
        var d = $("#sound_block>span[class='dubbling'][status='muted']").eq(0);
        var a = parseFloat(d.attr("start_time")) * 1000;
        var c = parseFloat(d.attr("len")) * 1000;
        sound_current_time = new Date().getTime();
        var b = sound_current_time - sound_start_time;
        if (b < c) {
            d.attr("status", "playing");
            log("[sound] play sound");
            sound_array.dubbling.setCurrentTime((a + b) / 1000);
            sound_array.dubbling.play()
        }
    }
};
