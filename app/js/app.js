/////Global functions////////

function initGlobal() {
	// var footerHeight = $("#footer").height();
	// $(".menu-aside").css("padding-bottom", footerHeight);
	// $(".menu-aside").css("margin-bottom", -footerHeight);
	// if($(window).width() < 900) {
	// 	$(".menu-aside").css("margin-bottom", "130px");
	// }
	function menuOpen() {
		if($(window).width() < 900) {
			$(".left-menu").addClass("menu__close");
		} else {
			$(".left-menu").removeClass("menu__close");
		}
		$(".mobile-menu-toggle").unbind('click').click(function() {
			$(".left-menu").toggleClass("menu__open");
			$(".mobile-menu").toggleClass("mobile-menu__open");
			if($(".left-menu").hasClass("menu__open")) {
				$(".content").css("margin-left", "200px");
				
				$(".main-logo").css("left", "200px");
				$("body").css("overflow-y", "hidden");
			} else {
				$(".content").css("margin-left", "0");
				$(".main-logo").css("left", "0");
				$("body").removeAttr('style');
			}

		});
		
	}
	function openWindow(window_src) {
		window.open(window_src, 'newwindow', config='height=100, width=400, '
	    + 'toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, '
	    + 'directories=no, status=no');
	}
	menuOpen();
	$(window).resize(function() {
		menuOpen();
		if($(window).width() < 900) {
			$(".menu-aside").css("margin-bottom", "130px");
		}
	});
	function goBack() {
	    window.history.back();
	}
	if ($('#back-to-top').length) {
	    var scrollTrigger = 100, // px
	        backToTop = function () {
	            var scrollTop = $(window).scrollTop();
	            if (scrollTop > scrollTrigger) {
	                $('#back-to-top').addClass('show');
	            } else {
	                $('#back-to-top').removeClass('show');
	            }
	        };
	    backToTop();
	    $(window).on('scroll', function () {
	        backToTop();
	    });
	    $('#back-to-top').on('click', function (e) {
	        e.preventDefault();
	        $('html,body').animate({
	            scrollTop: 0
	        }, 700);
	    });
	}
}


/////Index functions////////


function index() {
	initGlobal();
	$(".carousel").carousel({
		swipe: 30
	});
	var emptyCells, i;
	$('.index-books-wrap').each(function() {
		emptyCells = [];
		for (i = 0; i < $(this).find('.book-card').length; i++) {
			emptyCells.push($('<div>', {
			  class: 'empty'
			}));
		}
		$(this).append(emptyCells);
	});
	// function carouselNormalization() {
	// 	var items = $('#news-carousel .carousel-item'),
	// 	    heights = [],
	// 	    tallest;

	// 	if (items.length) {
	// 	    function normalizeHeights() {
	// 	        items.each(function() {
	// 	            heights.push($(this).height());
	// 	        });
	// 	        tallest = Math.max.apply(null, heights);
	// 	        items.each(function() {
	// 	            $(this).css('min-height',tallest + 'px');
	// 	        });
	// 	    };
	// 	    normalizeHeights();

	// 	    $(window).on('resize orientationchange', function () {
	// 	        tallest = 0, heights.length = 0;
	// 	        items.each(function() {
	// 	            $(this).css('min-height','0');
	// 	        });
	// 	        normalizeHeights();
	// 	    });
	// 	}
	// }
	// carouselNormalization();
	$.fn.carouselHeights = function() {
		var items = $(this),
	        heights = [],
	        tallest;
		var normalizeHeights = function() {
			items.each(function() {
	            heights.push($(this).height());
	        });
	        tallest = Math.max.apply(null, heights);
	        items.each(function() {
	            $(this).css("min-height",tallest + "px");
	        });
	    };
		normalizeHeights();
		$(window).on("resize orientationchange", function () {
	        tallest = 0;
	        heights.length = 0;
			items.each(function() {
	            $(this).css("min-height","0");
	        });
	        normalizeHeights();
	    });
	};
	$(window).on("load", function(){
        $("#news-carousel .carousel-item").carouselHeights();
    });
    $(window).on("load", function(){
        $(".tab-pane .carousel .carousel-item").carouselHeights();
    });
	function textLimit() {
		if ($(window).width() < 600) {
			$(".index-news").text(function(index, currentText) {
				if(currentText.length >= 70) {
					return currentText.substr(0, 70) + "...";
				} else {
					return currentText;
				}
			});
		} else {
			$(".index-news").text(function(index, currentText) {
				if(currentText.length >= 120) {
					return currentText.substr(0, 120) + "...";
				} else {
					return currentText;
				}
			});
		}
	}
	textLimit();
	$(window).resize(function() {
		textLimit();
	});
}



/////All Books functions////////


function allBooks() {
	initGlobal();
	var emptyCells, i;
	$('.all-books-card-wrap').each(function() {
		emptyCells = [];
		for (i = 0; i < $(this).find('.book-card').length; i++) {
			emptyCells.push($('<div>', {
			  class: 'book-card margin-lr-10px is-empty'
			}));
		}
		$(this).append(emptyCells);
	});
	var emptyCells, i;
	$('.index-books-wrap').each(function() {
		emptyCells = [];
		for (i = 0; i < $(this).find('.book-card').length; i++) {
			emptyCells.push($('<div>', {
			  class: 'empty'
			}));
		}
		$(this).append(emptyCells);
	});
	// $('#all-books-pagination-container').pagination({
	// 	dataSource: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
	// 	pageSize: 4,
	// });
	// $(".select-box__current").each(function() {
	// 	$(this).unbind('click').click(function() {
	// 		$(".select-box__current").removeClass('select-box__open');
	// 		$(this).toggleClass('select-box__open');
	// 	});
	// });
	// $(".select-box__list li").each(function() {
	// 	$(this).unbind('click').click(function() {
	// 		$(".select-box__current").removeClass('select-box__open');
	// 		$(".select-wrap .select-box__open + .select-box__list").css('opacity', '0');
	// 	});
	// });
	$(".filter-button").click(function(e) {
		e.preventDefault();
		$(".filter-form").slideToggle();
	});
	// if($(window).width() > 768) {
	// 	$(".filter-form").show();
	// }
	$(window).resize(function() {
		if($(window).width() > 768) {
			$(".filter-form").show();
		} else {
			$(".filter-form").hide();
		}
	});
	
}


/////Single Books functions////////


function bookSingle() {
	initGlobal();
}


/////Newslist functions////////


function newsList() {
	initGlobal();
	// $('#news-pagination-container').pagination({
	// 	dataSource: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
	// 	pageSize: 2,
	// });
	function textLimit() {
		if ($(window).width() < 600) {
			$(".news-content p").text(function(index, currentText) {
				if(currentText.length >= 70) {
					return currentText.substr(0, 70) + "...";
				} else {
					return currentText;
				}
			});
		} else {
			$(".news-content p").text(function(index, currentText) {
				if(currentText.length >= 120) {
					return currentText.substr(0, 120) + "...";
				} else {
					return currentText;
				}
			});
		}
	}
	textLimit();
	$(window).resize(function() {
		textLimit();
	});
}


/////News functions////////


function newsListSingle() {
	initGlobal();
}


/////Faq functions////////


function faq() {
	initGlobal();
}


/////Faq functions////////


function about() {
	initGlobal();
}


/////Faq functions////////


function sites() {
	initGlobal();
}


/////Faq functions////////


function history() {
	initGlobal();
	var emptyCells, i;
	$('.index-books-wrap').each(function() {
		emptyCells = [];
		for (i = 0; i < $(this).find('.book-card').length; i++) {
			emptyCells.push($('<div>', {
			  class: 'empty'
			}));
		}
		$(this).append(emptyCells);
	});
	let name = 'Eric';
}


/////Playbook functions////////


function playbook() {
	// var flip_delay: 5000,
	// 	preload_all: true,
	// 	bookdata_json_url: "001.json",
	// 	text: "en",
	// 	blank: "../images/transparent.png",
	// 	single_page_mode: true,
	// 	margin: {top:120, bottom:50, left:30, right:30}
	// $.ajax({
 //        contentType: "application/json; charset=utf-8",
 //        url: bookdata_json_url,
 //        dataType: "json"
 //    }).done(function(data) {
 //        log("[json] book_data.json loaded");
 //        bgmusic = data.bgmusic;
 //        dubbling_file = data.dubbling;
 //        page_array = data.pages;
 //        p_count = page_array.length;
 //        var text_type_array = ["none", "text_ch", "text_py", "text_en"];
 //        for (var i = 0; i < page_array.length; i++) {
 //            for (var j = 0; j < text_type_array.length; j++) {
 //                if (page_array[i][text_type_array[j]] == undefined || page_array[i][text_type_array[j]] == "") {
 //                    page_array[i][text_type_array[j]] = blank_page
 //                }
 //            }
 //        }
 //        // if (/Firefox/i.test(navigator.userAgent)) {
 //        //     var reg = /(\w+)\.mp3/;
 //        //     var matches;
 //        //     if (bgmusic != "") {
 //        //         bgmusic = bgmusic.replace(".mp3", ".ogg")
 //        //     }
 //        //     if (dubbling_file != "") {
 //        //         dubbling_file = dubbling_file.replace(".mp3", ".ogg")
 //        //     }
 //        //     var file = "";
 //        //     for (var i = 0; i < page_array.length; i++) {
 //        //         if (page_array[i]["mp3"] != undefined && page_array[i]["mp3"].length > 0) {
 //        //             file = page_array[i]["mp3"][0]["file"];
 //        //             if (file != undefined && file != "") {
 //        //                 page_array[i]["mp3"][0]["file"] = file.replace(".mp3", ".ogg")
 //        //             }
 //        //         }
 //        //     }
 //        // }
 //        var accumulated_time = 0;
 //        for (var i = 0; i < page_array.length; i++) {
 //            if (page_array[i]["mp3"] != undefined && page_array[i]["mp3"].length > 0) {
 //                page_array[i]["mp3"][0]["start_time"] = accumulated_time / 1000;
 //                var soundlen = parseFloat(page_array[i]["mp3"][0]["len"]) * 1000;
 //                accumulated_time += soundlen
 //            }
 //        }
 //        var i = 0;
 //        var img = page_array[i]["img"];
 //        var text = page_array[i][current_text_type];
 //        while (img == "") {
 //            i++;
 //            img = page_array[i]["img"];
 //            var text = page_array[i][current_text_type]
 //        }
 //        $("#left-img").attr("src", img);
 //        $("#left-img").hide();
 //        $("#left-img-text").attr("src", text);
 //        $("#left-img-text").hide();
 //        if (/iPad|iPhone|Android/i.test(navigator.userAgent)) {
 //            $(".flipbookcontent, .interactioncontent").hide();
 //            $("#btnPreload").show().on("click", function() {
 //                loadMusic();
 //                $("#btnPreload").remove()
 //            })
 //        } else {
 //            $("#btnPreload, #btnStart").remove();
 //            $("#wait_block").show();
 //            loadMusic();
 //            start()
 //        }
 //        if (!default_single) {
 //            togglePageMode()
 //        }
 //    }).fail(function(jqXHR, textStatus, errorThrown) {
 //        log("[json] book_data.js loading failed! " + textStatus + " " + errorThrown)
 //    });
	// $(".select-box__current").each(function() {
	// 	$(this).click(function() {
	// 		$(this).toggleClass('select-box__open');
	// 	});
	// });
	// function imageResize() {
	// 	var wh = $(window).height();
	// 	var ww = $(window).width();
	// 	var topMenuHeight = $(".top-menu").outerHeight();
	// 	var bottomMenuHeight = $(".bottom-menu").outerHeight();
	// 	var imageWidth = $(".image-placeholder").children().find("img").outerWidth();
	// 	var imageHeight = $(".image-placeholder").outerHeight();
	// 	$(".image-placeholder").children().find("img").height(wh - (topMenuHeight+bottomMenuHeight) - 30);
	// 	if(imageWidth > ww) {
	// 		$(".image-placeholder").children().find("img").css('width', '100%');
	// 		$(".image-placeholder").children().find("img").css('height', 'auto');
	// 	} else {
	// 		$(".image-placeholder").children().find("img").css('width', 'auto');
	// 	}

	// }
	// imageResize();
	// $(window).resize(function() {
	// 	imageResize();
	// });
	// $("#btnZoomIn").click(function() {
	// 	window.resizeBy(100, 100)
	// });
	// $("#btnZoomOut").click(function() {
	// 	var wh = $(window).height();
	// 	if(wh > 500) {
	// 		window.resizeBy(-100, -100);
	// 	} else {
	// 		return;
	// 	}
	// });
	// $('#btnPrint').bind('click',function() {
	// 	html2canvas(document.querySelector(".image-placeholder")).then(canvas => {
	// 	    popup = window.open();
	// 	    popup.document.write('<img src="' + canvas.toDataURL() + '" style="width:100%;" />');
	// 	    setTimeout(function(){
	// 			popup.focus();
	// 			popup.print();
	// 		}, 2000);
	// 	});
	// });
	// $("#btnFullScreen").click(function() {
	// 	$(this).toggleClass("active");
 //    	$(this).trigger("switch");
	// });
	// $("#btnFullScreen").on("switch", function(){
	// 	if ($(this).hasClass("active")) {
	// 		$("#btnFullScreen>img").attr("src", "images/no-fullscreen.svg");
	// 	} else {
	// 		$("#btnFullScreen>img").attr("src", "images/fullscreen.svg");
	// 	}
	// });
	// $("#btnPageMode").click(function() {
	// 	$(this).toggleClass("active");
	// 	$(this).trigger("switch");
	// });
	// $("#btnPageMode").on("switch", function(){
	// 	var pageNum = $(".right-img img").attr("src");
	// 	pageNum = pageNum.substring(0,pageNum.lastIndexOf("."));
	// 	pageNum = pageNum.substr(-2, 2);
	// 	if ($(this).hasClass("active")) {
	// 		$("#btnPageMode>p").html("單頁");
	// 		if(pageNum == "01");
	// 	} else {
	// 		$("#btnPageMode>p").html("跨頁");
	// 	}
	// });
}
